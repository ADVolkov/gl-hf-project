## This telegram bot is designed to track the users you need. 
The bot tells you when a person goes online, how long his session lasted, who likes him and writes comments.

You can find a bot by nickname - @steal_info_bot

## Bot commands:
```/start - start online monitoring
/stop - stop online monitoring
/help - show hel
/add - add user to monitoring list "/add Telegram_UserName or vk_id(link)"
/list - show added users
/clear - clear user list
/remove - remove user from list with position in list (to show use /list command)"/remove 1"
/setdelay - set delay between user check in seconds
/logs - display command log
/clearlogs - clear the command log file
/cleardata - reset configuration
/disconnect - disconnect bot
/getall - status',
/setpostscount - set posts grabbing count. must be integer between 1 and 100
```

To use our program you need to enter in the settings.py your api_hash and api_id(You can get them by the link: https://my.telegram.org/apps), bot_token(FatherBot), vk_token(look for yourself) and run main.py

