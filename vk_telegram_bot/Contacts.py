class Contact:
    def __init__(self, name):
        self.name = name
        self.online = None
        self.last_offline = None
        self.last_online = None
        self.all_time = 0

    def __str__(self):
        return f'{self.name}'


class TelegramContact(Contact):
    def __init__(self, *args, **kwargs):
        super(TelegramContact, self).__init__(*args, **kwargs)


class VkContact(Contact):
    def __init__(self, *args, **kwargs):
        super(VkContact, self).__init__(*args, **kwargs)
        self.posts = None
        self.subscriptions = None
