from urllib.parse import urlencode
import requests
import time
import json
import math


class User:
    def __init__(self, token=None):
        self.friends_id = []
        self.token = token
        self.api_version = '5.131'
        self.base_url = 'https://api.vk.com/method/'
        self.banch_count = 0

    def __find_friends(self, target_uid):
        params = {
            'access_token': self.token,
            'target_uid': target_uid,
            'v': self.api_version
        }

        url = self.base_url + 'friends.get'
        response = requests.get(url, params)
        if response.status_code == 200:
            self.friends_id = response.json()['response']['items']
            self.banch_count = math.ceil(len(self.friends_id) / 500)

    def getid(self, target_name):
        params = {
            'access_token': self.token,
            'user_ids': target_name,
            'v': self.api_version,
            'name_case': 'Nom',
            'fields': 'online'
        }
        url = self.base_url + 'users.get'
        response = requests.get(url, params)
        id = None
        if response.status_code == 200:
            try:
                id = response.json()['response'][0]['id']
                time.sleep(0.4)
            except:
                pass
        return id

    async def isonline(self, target_uid):
        params = {
            'access_token': self.token,
            'user_ids': target_uid,
            'v': self.api_version,
            'name_case': 'Nom',
            'fields': 'online'
        }
        url = self.base_url + 'users.get'
        response = requests.get(url, params)
        online = None
        if response.status_code == 200:
            try:
                online = [['offline', 'online'][i['online']] for i in response.json()['response']]
                time.sleep(0.4)
            except:
                pass
        return online

    async def get_posts(self, target_uid, count=5):
        params = {
            'access_token': self.token,
            'owner_id': target_uid,
            'v': self.api_version,
            'filter': 'all',
            'count': count
        }
        url = self.base_url + 'wall.get'
        response = requests.get(url, params)
        posts = None
        if response.status_code == 200:
            try:
                posts = [(i['comments']['count'], i['likes']['count']) for i in response.json()['response']['items']]
                time.sleep(0.4)
            except:
                pass
        return posts

    async def getsubscriptions(self, target_uid):
        params = {
            'access_token': self.token,
            'user_id': target_uid,
            'v': self.api_version,
        }
        url = self.base_url + 'wall.get'
        response = requests.get(url, params)
        posts = None
        if response.status_code == 200:
            try:
                posts = response.json()['response']['count']
                time.sleep(0.4)
            except:
                pass
        return posts
        
    def __find_groups(self, target_uid):
        user_groups = {}
        params = {
            'access_token': self.token,
            'user_id': target_uid,
            'v': self.api_version,
            'extended': 1
        }

        url = self.base_url + 'groups.get'
        response = requests.get(url, params)

        if response.status_code == 200:
            try:
                items_from_groups = response.json()['response']['items']
                user_groups = {item['id']: item['name'] for item in items_from_groups}
                time.sleep(0.4)
                print('.')
            except KeyError:
                pass
        return user_groups

    def __get_members(self, group_id):
        users_in_group = 0
        params = {
            'access_token': self.token,
            'group_id': group_id,
            'v': self.api_version
        }

        url = self.base_url + 'groups.getMembers'
        response = requests.get(url, params)
        if response.status_code == 200:
            try:
                users_in_group = response.json()['response']['count']
                time.sleep(0.4)
                print('.')
            except:
                pass
        return users_in_group

    def __group_analyse(self, friends_id, user_groups):
        friend_groups = []
        for friend in friends_id:
            friend_groups += list(self.__find_groups(friend).keys())
        s = set(user_groups.keys())
        s.intersection_update(set(friend_groups))
        return s
        # return set(user_groups.keys()) - set(friend_groups)

    def get_secret_group(self):
        group_info = []
        user_id = self.__get_user_id()
        self.__find_friends(user_id)
        groups = self.__find_groups(user_id)
        unique_groups = self.__group_analyse(self.friends_id, groups)
        for group in unique_groups:
            count = self.__get_members(group)
            group_info.append(dict(name=groups[group], gid=group, members_count=count))
        with open('groups.json', 'w', encoding='utf-8') as f:
            json.dump(group_info, f, ensure_ascii=False, indent=2)

    def __friends_in_group(self, group_id):
        params = {
            'access_token': self.token,
            'group_id': group_id,
            'v': self.api_version,
            'filter': 'friends'
        }
        url = self.base_url + 'groups.getMembers'
        response = requests.get(url, params)
        count_from_groups = 0
        if response.status_code == 200:
            try:
                count_from_groups = response.json()['response']['count']
                print(count_from_groups)
                print(response.json()['response']['items'])
                time.sleep(0.4)
                print('.')
            except KeyError:
                pass
        return count_from_groups

    def get_unique_groups(self):
        group_info = []
        unique_groups = []
        user_id = self.__get_user_id()
        groups = self.__find_groups(user_id)
        for group_id in groups.keys():
            friends_count = self.__friends_in_group(group_id)
            print(friends_count)
            print(group_id)
            if friends_count == 0:
                unique_groups.append(group_id)
        for group in unique_groups:
            count = self.__get_members(group)
            group_info.append(dict(name=groups[group], gid=group, members_count=count))
        with open('unique_groups.json', 'w', encoding='utf-8') as f:
            json.dump(group_info, f, ensure_ascii=False, indent=2)

    def __get_member_group(self, group_id):
        count = 0
        while True:
            if count >= self.banch_count:
                break
            is_member = False

            params = {
                'access_token': self.token,
                'group_id': group_id,
                'user_ids': ','.join(str(i) for i in self.friends_id[count*500:((count+1)*500)-1]),
                'v': self.api_version,
                'extended': 1
            }
            print(params['user_ids'])
            url = self.base_url + 'groups.isMember'
            response = requests.get(url, params)
            print(response.status_code)
            if response.status_code == 200:
                print(response.json())
                try:
                    members_from_groups = response.json()['response']
                    print(members_from_groups)
                    for member in members_from_groups:
                        if member['member'] == 1:
                            is_member = True
                            break
                    time.sleep(0.4)
                    print('.')
                except KeyError:
                    print('KeyError')
                    pass
            count += 1
            if is_member:
                break
        return is_member

    def third_method(self):
        user_id = self.__get_user_id()
        groups = self.__find_groups(user_id)
        self.__find_friends(user_id)
        unique_groups = []
        print(groups)
        group_info = []
        for group_id in groups.keys():
            is_member = self.__get_member_group(group_id)
            print(is_member)
            print(group_id)
            if is_member:
                unique_groups.append(group_id)
        for group in unique_groups:
            count = self.__get_members(group)
            group_info.append(dict(name=groups[group], gid=group, members_count=count))
        with open('special_groups.json', 'w', encoding='utf-8') as f:
            json.dump(group_info, f, ensure_ascii=False, indent=2)
