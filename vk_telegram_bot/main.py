from datetime import datetime, timedelta, timezone
from sys import argv, exit
from telethon import TelegramClient, events, connection
from telethon.tl.types import UserStatusRecently, UserStatusEmpty, UserStatusOnline, UserStatusOffline, PeerUser, \
    PeerChat, PeerChannel
from time import mktime, sleep
import os
import telethon.sync
from threading import Thread
import collections
from settings import *
from vk_user import User
from Contacts import VkContact, TelegramContact
# import socks

client = TelegramClient('data_thief', API_ID, API_HASH, proxy=("socks5", '127.0.0.1', 4444)) # proxy=(socks.SOCKS5, '47.90.132.228', 80)
client.connect()
client.start()
bot = TelegramClient('bot', API_ID, API_HASH).start(bot_token=BOT_TOKEN)
vk_api = User(token=VK_TOKEN)
data = {}

help_messages = ['/start - start online monitoring ',
                 '/stop - stop online monitoring ',
                 '/help - show help ',
                 '/add - add user to monitoring list "/add Telegram_UserName or vk_id"',
                 '/list - show added users',
                 '/clear - clear user list',
                 '/remove - remove user from list with position in list (to show use /list command)"/remove 1"',
                 '/setdelay - set delay between user check in seconds',
                 '/logs - display command log',
                 '/clearlogs - clear the command log file',
                 '/cleardata - reset configuration',
                 '/disconnect - disconnect bot',
                 '/getall - status',
                 '/setpostscount - set posts grabbing count. must be integer between 1 and 100']

print('running')


@bot.on(events.NewMessage(pattern='^/logs$'))
async def logs(event):
    str = ''
    with open('spy_log.txt', 'r') as file:
        str = file.read()
    await event.respond(str)


@bot.on(events.NewMessage(pattern='/clearlogs$'))
async def clearLogs(event):
    open('spy_log.txt', 'w').close()
    await event.respond('logs has been deleted')


@bot.on(events.NewMessage(pattern='^/clear$'))
async def clear(event):
    message = event.message
    id = message.chat_id
    data[id] = {}
    await event.respond('User list has been cleared')


@bot.on(events.NewMessage(pattern='^/help$'))
async def help(event):
    await event.respond('\n'.join(help_messages))


@bot.on(events.NewMessage())
async def log(event):
    message = event.message
    id = message.chat_id
    str = f'{datetime.now(timezone.utc).strftime(DATETIME_FORMAT)}: [{id}]: {message.message}'
    printToFile(str)
    # await bot.send_message(entity=entity,message=str)
    # await event.respond('cleared')


@bot.on(events.NewMessage(pattern='^/stop$'))
async def stop(event):
    message = event.message
    id = message.chat_id
    if id not in data:
        data[id] = {}
    user_data = data[id]
    user_data['is_running'] = False
    await event.respond('Monitoring has been stopped')


@bot.on(events.NewMessage(pattern='^/cleardata$'))
async def clearData(event):
    data.clear()
    await event.respond('Data has been cleared')


async def vk_spy(event, contacts_list):
    statuses = await vk_api.isonline(list(map(lambda x: x.name, contacts_list)))
    for num, contact in enumerate(contacts_list):
        print(contact)
        if statuses[num] == 'online':
            if not contact.online:
                contact.online = True
                contact.last_offline = datetime.now(timezone.utc) + timedelta(hours=3)
                was_offline = (contact.last_offline).strftime(DATETIME_FORMAT)
                await event.respond(f'{was_offline}: {contact.name} went online.')
        else:
            if contact.online:
                contact.online = False
                contact.last_online = datetime.now(timezone.utc) + timedelta(hours=3)
                contact.all_time += ((contact.last_offline - contact.last_online).seconds // 60) % 60
                was_online = (contact.last_online).strftime(DATETIME_FORMAT)
                await event.respond(f'{was_online} {contact.name} went offline. current online = {contact.all_time}')
        id = event.message.chat_id
        if id not in data:
            data[id] = {}
        user_data = data[id]
        if 'post_counts' not in user_data:
            user_data['post_counts'] = 5
        posts = await vk_api.get_posts(contact.name, user_data['post_counts'])
        if contact.posts is None:
            contact.posts = posts
        elif contact.posts != posts:
            contact.posts = posts
            await event.respond(f'{contact.name} changed their posts/comments/likes tier. new tier is {os.linesep.join(map(lambda x: f"comments = {x[0]}likes={x[1]}", posts))}')
        subscriptions = await vk_api.getsubscriptions(contact.name)
        if contact.subscriptions is None:
            contact.subscriptions = subscriptions
        elif contact.subscriptions != subscriptions:
            contact.subscriptions = subscriptions
            await event.respond(
                f'{contact.name} changed their number of subscriptions. new number of subscriptions is {subscriptions}')


@bot.on(events.NewMessage(pattern='^/start$'))
async def start(event):
    message = event.message
    id = message.chat_id
    if id not in data:
        data[id] = {}
    user_data = data[id]
    if ('is_running' in user_data) and (user_data['is_running']):
        await event.respond('Spy is already started')
        return

    if 'contacts' not in user_data:
        user_data['contacts'] = []

    contacts = user_data['contacts']

    if len(contacts) < 1:
        await event.respond('No contacts added')
        return
    await event.respond('Monitoring has been started')

    counter = 0
    user_data['is_running'] = True

    while True:
        user_data = data[id]
        if (not user_data['is_running']) or (len(contacts) < 1):
            break
        print(f'running {id}: {counter}')
        counter += 1
        vk_users = list(filter(lambda x: isinstance(x, VkContact), contacts))
        if vk_users:
            await vk_spy(event, vk_users)
        for contact in filter(lambda x: isinstance(x, TelegramContact), contacts):
            print(contact)
            if isinstance(contact, TelegramContact):
                entity = await client.get_input_entity(contact.name)
                account = await client.get_entity(entity)
                if isinstance(account.status, UserStatusOnline):
                    if not contact.online:
                        contact.online = True
                        contact.last_offline = datetime.now(timezone.utc) + timedelta(hours=3)
                        was_offline = (contact.last_offline).strftime(DATETIME_FORMAT)
                        await event.respond(f'{was_offline}: {contact.name} went online.')
                elif isinstance(account.status, UserStatusOffline):
                    if contact.online:
                        contact.online = False
                        last_time_online = utc2localtime(account.status.was_online)
                        if last_time_online is None:
                            last_time_online = datetime.now(timezone.utc) + timedelta(hours=3)
                        contact.last_online = account.status.was_online
                        contact.all_time += ((contact.last_offline - contact.last_online).seconds // 60) % 60
                        was_online = (last_time_online).strftime(DATETIME_FORMAT)
                        await event.respond(f'{was_online} {contact.name} went offline. current online = {contact.all_time}')
                    contact.last_offline = None
                else:
                    if contact.online:
                        contact.online = False
                        contact.last_online = datetime.now(timezone.utc) + timedelta(hours=3)
                        was_online = (contact.last_offline).strftime(DATETIME_FORMAT)
                        contact.all_time += ((contact.last_offline - contact.last_online).seconds // 60) % 60
                        await event.respond(f'{was_online}: {contact.name} went offline.  current online = {contact.all_time}')
                        contact.last_offline = None
        if 'delay' in user_data:
            delay = user_data['delay']
        else:
            delay = 5
        sleep(delay)
    user_data['is_running'] = False
    await event.respond(f'Spy gonna sleep')


@bot.on(events.NewMessage(pattern='^/add'))
async def add(event):
    message = event.message
    person_info = message.message.split()
    print(person_info)
    name = person_info[1]
    id = message.chat_id
    if id not in data:
        data[id] = {}
    user_data = data[id]
    if 'contacts' not in user_data:
        user_data['contacts'] = []
    contacts = user_data['contacts']
    if name.isdigit():
        contact = VkContact(name)
    elif 'vk.com' in name:
        contact = VkContact(vk_adaptor(name))
    else:
        contact = TelegramContact(name)
    contacts.append(contact)
    await event.respond(f'{name}: has been added')


@bot.on(events.NewMessage(pattern='^/remove'))
async def remove(event):
    message = event.message
    person_info = message.message.split()
    print(person_info)
    index = int(person_info[1])
    id = message.chat_id
    if id not in data:
        data[id] = {}
    user_data = data[id]
    if 'contacts' not in user_data:
        user_data['contacts'] = []
    contacts = user_data['contacts']
    if len(contacts) > index:
        del contacts[index]
        await event.respond(f'User №{index} has been deleted')
    else:
        await event.respond('Incorrect index')


@bot.on(events.NewMessage(pattern='^/setdelay'))
async def setDelay(event):
    message = event.message
    person_info = message.message.split()
    print(person_info)
    try:
        index = int(person_info[1])
    except Exception as ex:
        print(ex)
        await event.respond('Incorrect delay')
    else:
        id = message.chat_id
        if id not in data:
            data[id] = {}
        user_data = data[id]

        print(index)
        if (index >= 0):
            user_data['delay'] = index
            await event.respond(f'Delay has been updated to {index}')
        else:
            await event.respond('Incorrect delay')


@bot.on(events.NewMessage(pattern='^/setpostscount'))
async def setPostsCount(event):
    message = event.message
    info = message.message.split()
    print(info)
    try:
        count = int(info[1])
    except Exception as ex:
        print(ex)
        await event.respond('Incorrect grabbing count')
    else:
        id = message.chat_id
        if id not in data:
            data[id] = {}
        user_data = data[id]
        print(count)
        if (count > 0) and (count <= 100):
            user_data['post_counts'] = count
            await event.respond(f'Posts grabbing count has been updated to {count}')
        else:
            await event.respond('Incorrect grabbing count. must be between 1 and 100')


@bot.on(events.NewMessage(pattern='^/disconnect$'))
async def disconnect(event):
    await event.respond('Bot gonna disconnect')
    await bot.disconnect()


@bot.on(events.NewMessage(pattern='/list'))
async def list_contact(event):
    message = event.message
    id = message.chat_id
    if id not in data:
        data[id] = {}
    user_data = data[id]

    if 'contacts' not in user_data:
        user_data['contacts'] = []
    contacts = user_data['contacts']
    response = 'List is empty'
    if len(contacts):
        response = 'User list: \n' + '\n'.join([str(x) for x in contacts])
    await event.respond(response)


@bot.on(events.NewMessage(pattern='/getall'))
async def getAll(event):
    response = ''
    for key, value in data.items():
        response += f'{key}:\n'
        for j, i in value.items():
            if isinstance(i, collections.Sequence):
                response += f'{j}: ' + '\n'.join([str(x) for x in i]) + '\n'
            else:
                response += f'{j}: {i}\n'
        response += '\n'
    await event.respond(response)


def main():
    bot.run_until_disconnected()


def vk_adaptor(url):
    return vk_api.getid(url[url.rfind(r'/')+1:])


def utc2localtime(utc):
    pivot = mktime(utc.timetuple())
    offset = datetime.fromtimestamp(pivot) - datetime.utcfromtimestamp(pivot)
    return utc + offset


def printToFile(str):
    file_name = 'spy_log.txt'
    with open(file_name, 'a') as f:
        print(str)
        f.write(str + '\n')


def get_interval(date):
    d = divmod(date.total_seconds(), 86400)  # дни
    h = divmod(d[1], 3600)  # часы
    m = divmod(h[1], 60)  # минуты
    s = m[1]  # секунды
    return '%dh:%dm:%ds' % (h[0], m[0], s)


if __name__ == '__main__':
    main()


